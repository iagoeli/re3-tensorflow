import setuptools

setuptools.setup(
    name="re3",
    version="0.0.1",
    author="danielgordon10",
    description="RE3 Tracker",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
    ],
)
